<?php

namespace AppBundle\Entity;
use FR3D\LdapBundle\Model\LdapUserInterface;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{




    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var text
     *
     * @ORM\Column(name="photo", type="text",nullable=true)
     */
    protected $photo;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }




        /**
         * Get photo
         *
         * @return text
         */
    public function getPhoto()
    {
        return $this->photo;
    }



    /**
     * Set photo
     *
     * @param text $photo
     *
     * @return Client
     */
    public function setPhoto($photo)
    {
        $this->photo =base64_encode($photo);

        return $this;
    }


    /**
     * Set userName
     *
     * @param string $userName
     *
     * @return User
     */
  //  public function setUserName($username)
   // {
    //    $this->username = $username;

      //  return $this;
   // }

    /**
     * Get userName
     *
     * @return string
     */
    // public function getUserName()
    // {
      //  return $this->username;
   // }
}

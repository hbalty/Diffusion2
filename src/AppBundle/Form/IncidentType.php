<?php

namespace AppBundle\Form;

use AppBundle\Entity\Incident;
use AppBundle\Entity\Metier;
use AppBundle\Entity\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use AppBundle\Form\ImpactMetierType;
use AppBundle\Form\ImpactApplicationType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class IncidentType extends AbstractType
{

    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('client',EntityType::class,array(
          'class' => 'AppBundle:Client',
          'multiple' => false,
          'expanded' => false,
          'required' => true,
          'placeholder' => 'Selectionner un client',
          'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u');
            },
            'choice_label' => 'clientName',

        ))
        ->add('Titre',TextType::class)
        ->add('DateDebut',DateTimeType::class)
        ->add('dureeResolution',TextType::class)
        ->add('DateFin',DateTimeType::class)
        ->add('DateDebutImpact',DateTimeType::class,array(
        'placeholder' => array(
        'year' => 'Year', 'month' => 'Month', 'day' => 'Day'
         )
        ))
        ->add('DateFinImpact',DateTimeType::class)
       ->add('Impact',EntityType::class,array(
          'class' => 'AppBundle:Impact',
          'multiple' => false,
          'expanded' => false,
          'required' => true,
          'placeholder' => "selectionner niveau d'impact",
          'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u');
            },
            'choice_label' => 'nomImpact',
            'choice_name' => 'nomImpact'

        ))
        ->add('Type',ChoiceType::class,array(
          'choices' => array(
            'Exploitation' => 'Exploitation',
            'Changement' => 'Changement',
            'Extérieur' => 'Extérieur'
          ),
          'multiple' => false,
          "expanded" => true ,
        ))
        ->add('descTechnique',TextareaType::class)
        ->add('ressentiUtilisateur',TextareaType::class)
        ->add('notes',TextareaType::class)
        ->add('ddiConnue',CheckboxType::class,array(
            'label' => "Date Debut incident Connue"
        ))
        ->add('dfiConnue',CheckboxType::class,array(
          'label' => "Date Fin incident Connue"
        ))
        ->add('ddimConnue',CheckboxType::class,array(
        'label' => "Date Fin incident Connue"
        ))
        ->add('dfimConnue',CheckboxType::class,array(
        'label' => "Date Fin incident Connue"
        ))
        ->add('isApplicationImpact',CheckboxType::class,array(
        'label' => "Impact Application"
        ))
       
        ->add('impactApplication',EntityType::class,array(
          'class' => 'AppBundle:Application',
          'multiple' => true,
          'expanded' => false,
          'required' => false,
          'label' => 'Choisissez un impact',
          'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u');
            },
            'choice_label' => 'nomApplication',

        ))
        ->add('impactMetier',EntityType::class,array(
          'class' => 'AppBundle:Metier',
          'multiple' => true,
          'expanded' => false,
          'required' => false,
          'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u');
            },
            'choice_label' => 'nomMetier',

        ));

    }


    /**
    * @param OptionsResolverInterface $resolver
    */

    public function setDefaultOptions(OptionsResolverInterface $resolver){
      $resolver->setDefaults(array(
        'data_class' => 'AppBundle\Entity\Incident'
      ));
    }


}

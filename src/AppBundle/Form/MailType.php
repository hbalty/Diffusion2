<?php

namespace AppBundle\Form;

use AppBundle\Entity\Metier;
use AppBundle\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\ImpactMetierType;
use AppBundle\Form\ImpactApplicationType;
use Doctrine\ORM\EntityRepository;

class MailType extends AbstractType
{

    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

        ->add('email',EmailType::class)
        ->add('description',TextareaType::class)
        ->add('department',TextareaType::class);



}

    /**
    * @param OptionsResolverInterface $resolver
    */

    public function setDefaultOptions(OptionsResolverInterface $resolver){
      $resolver->setDefaults(array(
        'data_class' => 'AppBundle\Entity\Mail'
      ));
    }


}

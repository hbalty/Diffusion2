<?php

namespace AppBundle\Form;

use AppBundle\Entity\Metier;
use AppBundle\Entity\Client;
use AppBundle\Entity\MailingList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class TemplateType extends AbstractType
{

    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('client',EntityType::class,array(
          'class' => 'AppBundle:Client',
          'multiple' => false,
          'expanded' => false,
          'required' => true,
          'placeholder' => "selectionner un client",
          'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u');
            },
            'choice_label' => 'clientName',

        ))
        ->add('titre',TextareaType::class)
        ->add('destinataires',TextareaType::class)
        ->add('destEnCopie',TextareaType::class)
        ->add('commentaire',TextareaType::class)
        ->add('sujet',TextareaType::class)
        ->add('categorie',EntityType::class,array(
          'class' => 'AppBundle:Categorie',
          'multiple' => true,
          'expanded' => false,
          'required' => true,
          'placeholder' => "Selectionner les catégories",
          'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u');
            },
            'choice_label' => 'nomCategorie',

        ))
        ->add('variables', CollectionType::class, array(
        'entry_type'   => VariableType::class,
        'allow_add'    => true))
        ->add('template',CKEditorType::class, array(
         'attr' => array(
            'id' => 'information_ckeditor',
            ),
        'config' => array(
        'uiColor' => '#ffffff',
        'height' => '500',
        'allowedContent' => true,
      )));

}

    /**
    * @param OptionsResolverInterface $resolver
    */

    public function setDefaultOptions(OptionsResolverInterface $resolver){
      $resolver->setDefaults(array(
        'data_class' => 'AppBundle\Entity\Template'
      ));
    }


}

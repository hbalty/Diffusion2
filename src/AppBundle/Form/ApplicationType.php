<?php

namespace AppBundle\Form;

use AppBundle\Entity\Application;
use AppBundle\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\ImpactMetierType;
use AppBundle\Form\ImpactApplicationType;
use Doctrine\ORM\EntityRepository;

class ApplicationType extends AbstractType
{

    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('nomApplication',TextType::class)
          ->add('metiers',EntityType::class,array(
              'class' => 'AppBundle:Metier',
              'multiple' => true,
              'expanded' => false,
              'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u');
            },
            'choice_label' => 'nomMetier',
          ))
          ->add('activationStatus',CheckboxType::class,array(
            'label' => 'Active',
            'required' => false));

}

    /**
    * @param OptionsResolverInterface $resolver
    */

    public function setDefaultOptions(OptionsResolverInterface $resolver){
      $resolver->setDefaults(array(
        'data_class' => 'AppBundle\Entity\Application'
      ));
    }


}

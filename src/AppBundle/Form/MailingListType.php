<?php

namespace AppBundle\Form;

use AppBundle\Entity\Metier;
use AppBundle\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\ImpactMetierType;
use AppBundle\Form\ImpactApplicationType;
use Doctrine\ORM\EntityRepository;

class MailingListType extends AbstractType
{

    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        //TODO ajouter le champs formulaire d'ajout de mailingList
        ->add('mailingListName',TextType::class)
        ->add('client',EntityType::class,array(
          'class' => 'AppBundle:Client',
          'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u');
          },
          'choice_label' => 'clientName',
          'placeholder' => 'selectionner un client',
          'required' => true,
        ))
        ->add('idMetier',EntityType::class,array(
          'class' => 'AppBundle:Metier',
          'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u');
          },
          'choice_label' => 'nomMetier',
        ))
        ->add('impactLevel',ChoiceType::class,array(
          'choices' => array(
            'Faible' => 1,
            'Moyen' => 2,
            'Fort'=> 3
          ),
          'multiple' => false,
          "expanded" => true ,
        ))
        ->add('mailTo',TextType::class)
        ->add('mailCC',EntityType::class,array(
          'class' => 'AppBundle:Mail',
          'multiple' => true,
          'expanded' => false,
          'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u');
          },
          'choice_label' => 'email',
        ));





}

    /**
    * @param OptionsResolverInterface $resolver
    */

    public function setDefaultOptions(OptionsResolverInterface $resolver){
      $resolver->setDefaults(array(
        'data_class' => 'AppBundle\Entity\MailingList'
      ));
    }


}

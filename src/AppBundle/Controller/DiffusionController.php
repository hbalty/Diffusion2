<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Doctrine\Common\Collections\ArrayCollection;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\Diffusion;
use AppBundle\Entity\MailingList;
use AppBundle\Entity\Incident;
use AppBundle\Entity\Client;
use AppBundle\Entity\InformationDiffusion;
use AppBundle\Entity\Template;
use AppBundle\Entity\Impact;
use AppBundle\Form\DiffusionType;
use AppBundle\Form\MailingListType;
use AppBundle\Form\InformationDiffusionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
class DiffusionController extends Controller
{
    /**
     * @Route("/diffusion/send/{idIncident}/{idClient}", name="diffusion_send")
     */

 public function sendAction(Request $request,$idIncident, $idClient)
    {


            // Initialisation
            $incident = new Incident();
            $client = new Client();
            $impact = new Impact();

            /*  Récupération de l'incident en question  */

            $incident = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Incident')
            ->findOneById($idIncident);

             /*  Récupération de l'impact en question  */
             $impact = $incident->getImpact();


            /*  Récupération du client en question  */

            $client = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Client')
            ->findOneById($idClient) ;

            // Récupération des Metiers/Applications Impactés
            $lesListesDeDiffusions  = "";

            if ($incident->getIsApplicationImpact())
            {
                $impactMetiers = $incident->getImpactMetier()->getValues();
                foreach($impactMetiers as $impactMetier){
                 $lesListesDeDiffusions.=';'.$impactMetier->getMailinglists() ;
                }
            } else {

               $impactApplications = $incident->getImpactApplication()->getValues();
               foreach($impactApplications as $impactApplication){
                    foreach($impactApplication->getMetiers() as $metier){
                     $lesListesDeDiffusions.=';'.$metier->getMailingLists();
                    }
               }

            }

            // transformations des listes de diffusions en tableau

            $tableauDeDiffusion = array_unique(explode(';',$lesListesDeDiffusions));
            $validatedEmails = array();
            $i = 0;

            foreach($tableauDeDiffusion as $email){
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $validatedEmails[$i] = $email;
                $i++ ;
              }

            }
            $tableauMailsImpacts = explode(';',$incident->getImpact()->getMailingList());
            $impactValidatedEmails = array();

            $i = 0 ;
            foreach($tableauMailsImpacts as $email){
              if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $impactValidatedEmails[$i] = $email;
                $i++ ;
              }
            }


          self::formatTemplate($client->getTemplate(),$incident,$client) ;

            // instantiation d'une diffusion

            $diffusion = new Diffusion();
            $diffusion->setClient($client);
            $diffusion->setIncident($incident);
            $diffusion->setContenu(self::formatTemplate($client->getTemplate(),$incident,$client));
            $diffusion->setDestEnCopie(implode(';',$validatedEmails));
            $diffusion->setDestImpact(implode($impactValidatedEmails));
            $diffusion->setSujet($client->getTemplateSubject());


             $form = $this->createForm(DiffusionType::class,$diffusion);

               $form -> handleRequest($request);
              if ($form->isSubmitted() &&  $form->isValid() ){
              $data = $form->getData();
              $diffusion->setDateEnvoi(new \DateTime());
              $diffusion->setDestEnCopie($data->getDestEnCopie());

              $em = $this->getDoctrine()
              ->getManager();

              $em-> persist($diffusion);
              $em->flush();

              $message = \Swift_Message::newInstance()
               ->setSubject('Hello Email')
               ->setFrom('balty.houssem@gmail.com')
               ->setTo("houssem.balti@cdiscount.com")
               ->setBody($data->getContenu(),'text/html') ;

               $this->get('mailer')->send($message);

            }


            return  $this->render('diffusion/diffusionAdd.html.twig', array(
              'addForm' => $form->createView(),
               'lesMetiers' => $validatedEmails,
               'totalMail' => array_merge($validatedEmails,$tableauMailsImpacts),
                ));
    }



    public  function formatTemplate($template,$incident, $client){

         $template = str_replace('{{ titre }}', $incident->getTitre(),$template) ;
         $template =str_replace('{{ client }}',$client->getClientName(),$template) ;
         $template =str_replace('{{ statut }}', $incident->getStatut(),$template);
         $template = str_replace('{{ impacts }}',self::listMetier($incident),$template);
         $template = str_replace('{{ duree }}',$incident->getDureeResolution(),$template);


         if ($incident->getDateDebut() != null )
          $template = str_replace('{{ dateDebut }}',$incident->getDateDebut()->format('Y-m-d H:i:s'),$template);
          else
          $template = str_replace('{{ dateDebut }}',"Date non connue",$template);

          if ($incident->getDateFin() != null)
            $template = str_replace('{{ dateFin }}',$incident->getDateFin()->format('Y-m-d H:i:s'),$template);
          else
          $template = str_replace('{{ dateFin }}',"Date non connue",$template);

         $template = str_replace('{{ ressentiUtilisateur }}',$incident->getRessentiUtilisateur(),$template);
         $template = str_replace('{{ desc }}',$incident->getDescTechnique(),$template);

         return $template;



    }

    public function listMetier($incident){
     $table = "<tr>" ;
     if ($incident->getIsApplicationImpact()){
       foreach($incident->getImpactMetier() as $impactMetier ){
        $table .="<tr class='impacts'> <td style='height: 40px;
                border-bottom: 1px'> ".$impactMetier->getNomMetier()."</td>  <td style='height: 40px;
                border-bottom: 1px'> <img style='width: 30%;' src='http://www.navocap.com/custom/img/navocap/suivi-client-SAEIV.png' /> </td> </tr> " ;
       }
     } else {
        foreach ($incident->getImpactApplication() as $impactApplication){
        $table .="<tr> <td style='height: 40px;
                border-bottom: 1px' >  Application  ".$impactApplication->getMetiers()->get(0)->getNomMetier()."</td>  <td style='height: 40px;
                border-bottom: 1px'>  <img  style='width: 30%;' src='http://www.navocap.com/custom/img/navocap/suivi-client-SAEIV.png' /> </td> </tr> " ;
        }
       }

     return $table ;

    }

    /**
     * @Route("/diffusion/send/{idTemplate}", name="template_diffusion_send")
     */

 public function templateSendAction(Request $request,$idTemplate)
    {
      $variables = new ArrayCollection();
      $client = new Client();
      $template = $this->getDoctrine()->getManager()->getRepository('AppBundle:Template')->findOneById($idTemplate);
      $diffusion = new InformationDiffusion();
      $diffusion->setClient($template->getClient());
      $diffusion->setSujet($template->getSujet());
      $diffusion->setDestinataire($template->getDestinataires());
      $diffusion->setDestEnCopie($template->getDestEnCopie());
      $diffusion->setContenu($template->getTemplate());
      $diffusion->setTemplate($template);
    
      foreach ($template->getVariables() as $key => $value) {
         $variables[$value->getName()] = "" ;
      }
      $diffusion->setVariables($variables);
      $diffusion->setEmetteur($this->getUser());
      $form = $this->createForm(InformationDiffusionType::class,$diffusion);
      $form->handleRequest($request) ;
      
      if ($form->isSubmitted() &&  $form->isValid() ){
      $diffusion = $form->getData();
      // preparation du contenu
       $message = \Swift_Message::newInstance()
               ->setSubject('Hello Email')
               ->setFrom('balty.houssem@gmail.com')
               ->setTo("houssem.balti@cdiscount.com")
               ->setBody($diffusion->getContenu(),'text/html') ;
               
       $this->get('mailer')->send($message);
       $em = $this->getDoctrine()->getManager();
       $em-> persist($diffusion);
       $em->flush();
      
    }



       return  $this->render('diffusion/informationDiffusionSend.html.twig', array(
       'addForm' => $form->createView(),
        ));
    }
    
    


    
     /**
     * @Route("/information/diffusion/history", name="information_diffusion_history")
     */

 public function informationHistoryAction(Request $request)
    {
       // $history = $this->getDoctrine()->getRepository('AppBundle:InformationDiffusion')
       //->createQueryBuilder('c')
       //->getQuery()
       //->iterate();
       
        $qb = $this->getDoctrine()
                   ->getManager()
                   ->createQueryBuilder() ;
          
          $qb->select(array('u'))
        ->from('AppBundle:InformationDiffusion', 'u');
        
        $history = $qb->getQuery()->getResult();
       
       return  $this->render('diffusion/history.html.twig', array(
       'history' => $history,
        ));
       
           
    }
    
    
         /**
     * @Route("/information/diffusion/history/{idTemplate}", name="information_diffusion_client_history")
     */

 public function informationHistoryTemlpateAction(Request $request, $idTemplate)
    {
     
             $template =  $this
            ->getDoctrine()
            ->getRepository('AppBundle:Template')
            ->findOneById($idTemplate);
      
         $qb = $this->getDoctrine()
                   ->getManager()
                   ->createQueryBuilder() ;
          
          $qb->select(array('u'))
        ->from('AppBundle:InformationDiffusion', 'u')
        ->where('u.template = :template')
        ->setParameter('template',$template) ;
        
        
        $history = $qb->getQuery()->getResult();
       
       
       return  $this->render('diffusion/history.html.twig', array(
       'history' => $history,
        ));
       
           
    }






}

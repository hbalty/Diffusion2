<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Metier;
use AppBundle\Entity\Client;
use Doctrine\ORM\Query\Expr;
use AppBundle\Form\MetierType;
use AppBundle\Form\MailingListType;

class MetierController extends Controller
{
    /**
     * @Route("/metier/list", name="metier_list")
     */

  public function listAction(Request $request)
    {
        $metiers = $this
        ->getDoctrine()
        ->getManager()
       ->getRepository('AppBundle:Metier')
       ->createQueryBuilder('c')
       ->getQuery()->iterate();
    	 return $this->render("metier/metier.html.twig",array(
       'metiers' => $metiers,
      ));
    }



		 /**
     * @Route("/metier/details/{idMetier}", name="metier_details")
     */

  public function detailsAction(Request $request, $idMetier)
    {

      $metier = $this
			->getDoctrine()
			->getRepository('AppBundle:Metier')
			->findOneById($idMetier);

    	return $this->render("metier/metierDetails.html.twig",array(
        'metier' => $metier,
      ));
    }


    /**
     * @Route("/metier/add", name="metier_add")
     */

  public function addAction(Request $request)
    {
			
			   $metiers = $this
        ->getDoctrine()
        ->getManager()
       ->getRepository('AppBundle:Metier')
       ->createQueryBuilder('c')
       ->getQuery()->iterate();
					
         $metier = new Metier();
         $form = $this->createForm(MetierType::class,$metier);
         $form->handleRequest($request);
				 
         if ($form->isSubmitted() && $form->isValid()){
						$metier = $form->getData();
						// Gestion des erreurs existant dane le formulaire
            $em = $this->getDoctrine()->getManager();
            $em->persist($metier) ;
            $em->flush();
						return $this->render("metier/metierAdd.html.twig",array(
							'success' => 'Le metier est ajouté avec succès ! ',
							'form' => $form->createView(),
					   'metiers' => $metiers,
				 )) ;

         } else if ($form->isSubmitted() && !$form->isValid()){
					  $metier= $form->getData();
						$validator = $this->get('validator') ;
						$errors = $validator->validate($metier) ;
						return $this->render("metier/metierAdd.html.twig",array(
           'form' => $form->createView(),
					 'metiers' => $metiers,
					 'errors' => $errors,
         ));
						
				 }

         return $this->render("metier/metierAdd.html.twig",array(
           'form' => $form->createView(),
					 'metiers' => $metiers,
         ));

    }


        /**
         * @Route("/metier/update/{idMetier}", name="metier_update")
         */

      public function updateAction(Request $request, $idMetier)
        {
            $metier = $this->getDoctrine()
              ->getRepository('AppBundle:Metier')
              ->findOneById($idMetier);
              if (!$metier){
               
							 return $this->render("metier/metierUpdate.html.twig",array(
              'form' => $form->createView(),
							'errors' => "Ce metier n'existe pas ! "
            ));
              }
							
             $form = $this->createForm(MetierType::class,$metier);
             $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()){
               $metier = $form->getData();
               $em = $this->getDoctrine()->getManager();
               $em->flush();
                return $this->render("metier/metierUpdate.html.twig",array(
              'form' => $form->createView(),
							'success' => "Metier bien mis à jour ! ",
							));
            }  else if ($form->isSubmitted() && !$form->isValid()){
					  $metier= $form->getData();
						$validator = $this->get('validator') ;
						$errors = $validator->validate($metier) ;
						return $this->render("metier/metierUpdate.html.twig",array(
           'form' => $form->createView(),
					 'errors' => $errors,
         ));
						
				 }

            return $this->render("metier/metierUpdate.html.twig",array(
              'form' => $form->createView(),
            ));
        }
}

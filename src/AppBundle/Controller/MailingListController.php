<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\Application;
use AppBundle\Entity\MailingList;
use AppBundle\Form\ApplicationType;
use AppBundle\Form\MailingListType;

class MailingListController extends Controller
{
    /**
     * @Route("/mailinglist/list", name="mailinglist_list")
     */

 public function listAction(Request $request)
    {

          $mailinglists = $this
          ->getDoctrine()
          ->getManager()
          ->getRepository('AppBundle:MailingList')
          ->createQueryBuilder('c')
          ->getQuery()
          ->iterate();

          return $this->render('mailingList/mailingList.html.twig', array('mailingLists' => $mailinglists));

    }

     

    /**
     * @Route("/mailinglist/details/{id}", name="mailinglist_details")
     */

 public function detailsAction(Request $request,$id)
    {
      $mailinglist = $this->getDoctrine()
      ->getRepository('AppBundle:MailingList')
      ->findOneById($id);

      return $this->render('/mailingList/mailingListDetails.html.twig',array(
       'mailinglist' => $mailinglist,
       ));
    }


    /**
     * @Route("/mailinglist/add", name="mailinglist_add")
     */

 public function addAction(Request $request)
    {
      // Création d'un objet MailingList
      $mailingList = new MailingList();

       // Génération du formulaire de login !
       $form = $this->createForm(MailingListType::class,$mailingList);


      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $em = $this->getDoctrine()->getManager() ;
             $em->persist($data);
             $em->flush();
           }

      return $this->render('/mailingList/mailingListAdd.html.twig',array(
       'form' => $form->createView(),
       ));


    }

    /**
     * @Route("/mailinglist/delete/{id}", name="mailinglist_delete")
     */

  public function deleteAction($id)
    {
        $mailinglist = $this->getDoctrine()
        ->getRepository('AppBundle:MailingList')
        ->findOneById($id);

        $em = $this->getDoctrine()
        ->getManager() ;
        $em ->remove($mailinglist) ;
        $em ->flush();

        return $this->redirectToRoute('mailinglist_list');
    }


    /**
     * @Route("/mailinglist/update/{id}", name="mailinglist_update")
     */

  public function updateAction(Request $request,$id)
    {
      // Création d'un objet MailingList
      $mailingList = $this->getDoctrine()
      ->getRepository('AppBundle:MailingList')
      ->findOneById($id);

       // Génération du formulaire de login !
       $form = $this->createForm(MailingListType::class,$mailingList);


      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $em = $this->getDoctrine()->getManager() ;
             $em->flush();
           }

      return $this->render('/mailingList/mailingListAdd.html.twig',array(
       'form' => $form->createView(),
       ));
    }

}

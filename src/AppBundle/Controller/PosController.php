<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\incident;
use AppBundle\Entity\Pos;
use AppBundle\Form\ApplicationType;
use AppBundle\Form\PosType;
class PosController extends Controller
{
    /**
     * @Route("/pos/list/{idIncident}", name="pos_list")
     */

 public function listAction(Request $request, $idIncident)
    {
        
      $poss = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Pos')
            ->createQueryBuilder('c')
            ->where("c.incident=".$idIncident)
            ->getQuery()->iterate();
            

          return $this->render('pos/posList.html.twig', array('poss' => $poss, 'incident' => $idIncident));

    }


    /**
     * @Route("/pos/add/{idIncident}", name="pos_add")
     */

 public function addAction(Request $request,$idIncident)
    {
      // Création d'un objet Login
       
       $pos= new Pos();  
       $form = $this->createForm(PosType::class,$pos);
        $recentPos = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Pos')
            ->createQueryBuilder('c')
            ->where("c.incident=".$idIncident)
            ->getQuery()->iterate();

     
      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $data->setUserDeclare($this->getUser()) ;
             $data->setIncident($this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident)) ;
             if ($data->getIsResolution()){
               $data->setNextPosDate(null) ;
             } else{
               $data->setResolutionDate(null) ;
             }
             $em = $this->getDoctrine()->getEntityManager() ;
             $em->persist($data);
             $em->flush();
             return $this->redirect('/pos/add/'.$idIncident) ;
             
           }
           
      return $this->render('/pos/posAdd.html.twig',array(
       'form' => $form->createView(),
       'poss' => $recentPos,
       ));


    }
    
    
    
    /**
     * @Route("/pos/add/{idIncident}/{idPos}", name="pos_update")
     */

 public function updateAction(Request $request,$idIncident,$idPos)
    {
      // Création d'un objet Login
       
       $pos= $this->getDoctrine()->getRepository('AppBundle:Pos')->findOneById($idPos);
       $form = $this->createForm(PosType::class,$pos);
       $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneById(3) ;
        $recentPos = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Pos')
            ->createQueryBuilder('c')
            ->where("c.incident=".$idIncident)
            ->getQuery()->iterate();

     
      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $data->setUserDeclare($user) ;
             $data->setIncident($this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident)) ;
             $em = $this->getDoctrine()->getEntityManager() ;
             $em->flush();
             
           }
           
      return $this->render('/pos/posAdd.html.twig',array(
       'form' => $form->createView(),
       'poss' => $recentPos,
       ));


    }
    
    
    
    
    
     /**
     * @Route("/pos/delete/{idIncident}/{idPos}", name="pos_delete")
     */

 public function deleteAction(Request $request,$idIncident, $idPos)
    {
        $pos = $this->getDoctrine()
        ->getRepository('AppBundle:Pos')
        ->findOneById($idPos) ;
        $em = $this->getDoctrine()->getManager();
        
        $em->remove($pos);
        $em->flush();
        
        return $this->redirectToRoute("pos_add",array('idIncident' =>$idIncident));
    }



}

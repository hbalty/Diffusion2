<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use AppBundle\Entity\ImpactMetier;
use AppBundle\Entity\Incident;
use AppBundle\Form\ImpactMetierType;

class ImpactMetierController extends Controller
{
    /**
     * @Route("/impactmetier/list", name="impactmetier_list")
     */

 public function listAction(Request $request)
    {
          $lesImpactMetier = $this->getDoctrine()
          ->getManager()
          ->getRepository('AppBundle:ImpactMetier')
          ->createQueryBuilder('d')
          ->getQuery()->iterate();

          

          return $this->render('impactMetier/impactMetierList.html.twig', array('lesImpactMetier' => $lesImpactMetier));

    }


    /**
     * @Route("/impactmetier/add", name="impactmetier_add")
     */

  public function addAction(Request $request){

        $defaultImpactMetier =array();
        $lesMetiers = $this->getDoctrine()
        ->getManager()
        ->getRepository('AppBundle:Metier')
        ->createQueryBuilder('d')
        ->getQuery()->iterate();

        foreach ($lesMetiers as $key => $value) {
          $defaultImpactMetier[$value[0]->getNomMetier()] = 0;
        }

      $impactMetier = new ImpactMetier();
      $impactMetier->setConfiguration($defaultImpactMetier);

      $form = $this->createForm(ImpactMetierType::class,$impactMetier);
      $form->add('submit',SubmitType::class,array(
        'label' => 'Ajouter Configuration'
      ));

      $form ->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()){
        $impact = $form->getData();
        $em = $this->getDoctrine()->getEntityManager() ;
        $em->persist($impact);
        $em->flush();
      }


  return $this->render('impactMetier/impactMetierAdd.html.twig',
   array('addForm' =>$form->createView() ));
  }

  /**
   * @Route("/impactmetier/update/{idConfig}", name="impactmetier_update")
   */

  public function updateAction($idConfig)
     {

       return $this->render('impactApplication/impactApplicationModify.html.twig',
        array('modifyForm' =>$ImpactApplicationForm->createView() ));

     }






}

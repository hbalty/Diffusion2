<?php

/* diffusion/emailTest.html.twig */
class __TwigTemplate_113e37becc1688741b41ad69b2c151e9139a2b14af47f7285909b71f8889b97a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html> 
    <head> 
        <title>  </title>
        <style>
            /********************************************** Configuration Initiale *********************************************************/ 
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}


ul li{
    list-style: none ;
    display: inline;
    margin-left: 20%;

}

html{
    background-color: #ddd;
    color: #555;
    font-family: 'Arial',sans-serif,'Lato';
    font-size: 15px;
    font-weight: 300;
    text-rendering: optimizeLegibility;

}



/************************************************** Reusable components **************************************************************/

header{
    border-bottom: 1px grey solid ;
}

.jauge{
    width: 30%;
}


.main-container{
    background-color: #fff;
    width: 50%;
    height: 100%;
    margin-top: 40px;
    margin-left: auto; 
    margin-right: auto;
    border: 1px black solid ;
    
   
    
}

td{
    height: 40px;
    border-bottom: 1px
}

.status-resolu{
    background-color: chartreuse;
    height: 10%;
    text-align: center;
    color: white;
    
}


.status-encours{
    background-color: orangered;
    height: 10%;
    text-align: center;
    color: white;
    
}


.impacts{
    text-align: center;
}

.client{
    width: 50%;
    text-align: center;
}


.mail-content{
    margin : 10px 10px 10px 10px ;

}

.mail-content h2{
text-align: center;
margin-bottom: 20px;

}

.logo{
    margin: 20px 0 20px 20px  ;
    width: 30%;
    height: auto;
}

.incident-info{
    width: 100%;
}



.incident-info : first-child{
    background-color: green;
    color: white;
}



.footer {
    background-color: rgb(231,54,20);
    color: white;
    margin-top: 20%;
    padding-top: 2%;
    padding-bottom: 2%;
}
            
        </style>
    
    </head>
    
    <body>
        <div class=\"main-container\"> 
            <header> 
                 <img class=\"logo\" src=\"http://img1.lesnumeriques.com/news/56/56759/cdiscount_logo.jpg\"/>
                
            </header>
            
            <div class=\"mail-content\">
                <h2> ";
        // line 140
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "titre", array()), "html", null, true);
        echo " </h2>
                
              
                </table>
                
                <div> 
                
                    <table>
                        <tr>
                            <td class=\"client\"> ";
        // line 149
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : null), "clientName", array()), "html", null, true);
        echo " </td>
                            <td class=\"status-resolu\"> <b> ";
        // line 150
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "statut", array()), "html", null, true);
        echo " </b> </td>
                        </tr>
                        
                        <tr>
                            <td colspan=\"2\">
                                Bonjour, <br> 
                                Nous recontrons actuellement un incident sur les metiers suivants :
                            </td>
                            
                        </tr>
                        
                         <tr> 
                        <th> Nom Metier </th>
                        <th> Niveau impact </th>
                        </tr>
                        ";
        // line 165
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["metiers"]) ? $context["metiers"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["metier"]) {
            // line 166
            echo "                         <tr class=\"impacts\"> 
                            <td> ";
            // line 167
            echo twig_escape_filter($this->env, $this->getAttribute($context["metier"], "nomMetier", array()), "html", null, true);
            echo " </td>
                            <td> <img class=\"jauge\" src=\"http://www.navocap.com/custom/img/navocap/suivi-client-SAEIV.png\"/> </td>
                        </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['metier'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 171
        echo "     
                        <tr> 
                            <td> Temps de résolution approximatif :</td>
                            <td>  ";
        // line 174
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "dureeResolution", array()), "html", null, true);
        echo " </td>
                        </tr>
                        
                        <tr> 
                            <td> Date de début : </td>
                            <td>  ";
        // line 179
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "dateDebut", array()), "F jS \\a\\t g:ia"), "html", null, true);
        echo " </td>
                        </tr>
                        
                         <tr> 
                            <td> Date de fin prévue : </td>
                            <td> ";
        // line 184
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "dateFin", array()), "F jS \\a\\t g:ia"), "html", null, true);
        echo " </td>
                        </tr>
                        
                        <tr> 
                            <td> Ressenti Utilisateur : </td>
                            <td> ";
        // line 189
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "ressentiUtilisateur", array()), "html", null, true);
        echo " </td>
                        </tr>
                        
                         <tr> 
                            <td> Descriptions Techniques : </td>
                            <td> ";
        // line 194
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "descTechnique", array()), "html", null, true);
        echo " </td>
                        </tr>
                        
                        
                       
                    </table>
                    
                    
                    
        
                </div>
                
            
            </div>
            
        <div class=\"footer\">
            <ul> 
                <li> Logo  </li>
                <li> Link   </li>
                <li> Whatever  </li>
            </ul>
            
            <br> 
            
        
        </div>
        </div>
    </body>

</html>";
    }

    public function getTemplateName()
    {
        return "diffusion/emailTest.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 194,  240 => 189,  232 => 184,  224 => 179,  216 => 174,  211 => 171,  201 => 167,  198 => 166,  194 => 165,  176 => 150,  172 => 149,  160 => 140,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "diffusion/emailTest.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\diffusion\\emailTest.html.twig");
    }
}

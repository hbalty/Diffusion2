/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
// config.uiColor = '#AADC6E';

config.contentsCss = '/view/assets/ckeditor/fonts.css';

config.contentsCss = '/view/assets/ckeditor/fonts.css';
config.extraPlugins='backgrounds'; 
config.height = '600';
config.width = '800';

// Fonts
var fonts = config.font_names.split(';');
	fonts.push('Grobold/grobold');
	fonts.push('Futura/futura');
	fonts.push('Montserrat/Montserrat, Arial, Helvetica, sans-serif');
	fonts.push('Montserrat Light/Montserrat Light, Arial, Helvetica, sans-serif');

config.font_names = fonts.sort().join(';');

};
